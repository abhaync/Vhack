import os
import sys

'''Script to separate all words of sizes atleast 2 but not more than 7'''

legal_words = open('words.txt',r')
new_words = open('temp','w')

while 1:
	line = legal_words.readline()
	leng = len(line)
	if not line:
		break
	if leng >= 2 and leng <= 7:
		new_words.write(line)

legal_words.close()
new_words.close()
